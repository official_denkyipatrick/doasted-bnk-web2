import { Component, OnInit } from '@angular/core';
import { MatSnackBar } from '@angular/material/snack-bar';
import { AdminService } from 'src/app/services/admin.service';
import { Bank } from '../../../models/bank.interface';

@Component({
  selector: 'app-banks',
  templateUrl: './banks.component.html',
  styleUrls: ['./banks.component.scss']
})
export class BanksComponent implements OnInit {
  banks: Bank[] = [];
  isLoading: boolean = false;
  isErrorLoading: boolean = false;

  constructor(private adminService: AdminService, private snackBar: MatSnackBar) { }

  ngOnInit(): void {
    this.fetchBanks();
  }

  fetchBanks() {
    this.isLoading = true;
    this.isErrorLoading = false;

    this.adminService.getBanks()
    .subscribe(banks => {
      this.isLoading = false;
      this.banks = banks;
    }, error => {
      this.isLoading = false;
      this.isErrorLoading = true;
    });
  }

  deleteBank(bankId: string, bankName: string) {
    this.adminService.deleteBank(bankId)
    .subscribe(() => {
      this.banks = this.banks.filter(bank => bank.id != bankId);
      this.snackBar.open(`${bankName} is deleted`, 'OKAY', { duration: 5000 });
    }, error => {
      this.snackBar.open(`Unable to delete ${bankName}`, '', { duration: 5000 });
    });
  }

  OnBankCreated(bank: Bank) {
    this.banks.push(bank);
  }

}
