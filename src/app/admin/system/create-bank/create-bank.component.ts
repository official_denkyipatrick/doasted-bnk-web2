import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Bank } from 'src/app/models/bank.interface';
import { BankType } from 'src/app/models/create-bank-dto';
import { AdminService } from 'src/app/services/admin.service';

@Component({
  selector: 'app-create-bank',
  templateUrl: './create-bank.component.html',
  styleUrls: ['./create-bank.component.scss']
})
export class CreateBankComponent implements OnInit {
  bankForm: FormGroup;
  isCreating: boolean = false;
  isErrorCreating: boolean = false;
  bankTypes = [BankType.CRYPTO, BankType.NORMAL];
  @Output() created: EventEmitter<Bank> = new EventEmitter();

  

  constructor(private adminService: AdminService) { }

  ngOnInit(): void {
    this.bankForm = new FormGroup({
      name: new FormControl('', [Validators.required]),
      type: new FormControl('', [Validators.required])
    })
  }

  createBank() {
    this.isCreating = true;
    this.isErrorCreating = false;

    this.adminService.createBank({
      name: this.bankForm.get('name').value,
      type: this.bankForm.get('type').value
    })
    .subscribe(bank => {
      this.isCreating = false;
      this.created.emit(bank);
    }, error => {
      this.isCreating = false;
      this.isErrorCreating = true;
    })
  }

}
