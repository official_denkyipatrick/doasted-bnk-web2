import { Component, Inject, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Bank } from '../../models/bank.interface';
import { OkDialogComponent } from '../../ok-dialog/ok-dialog.component';
import { CommonService } from '../../services/common.service';
import { UtilityService } from '../../services/utility.service';

@Component({
  selector: 'app-withdrawal-dialog',
  templateUrl: './withdrawal-dialog.component.html',
  styleUrls: ['./withdrawal-dialog.component.scss']
})
export class WithdrawalDialogComponent implements OnInit {
  form: FormGroup;
  banks: Bank[] = [];
  parsedBalance: string;
  errorMessage: string = '';
  isWithdrawing: boolean = false;
  selectedCurrency = JSON.parse(localStorage.getItem('currency'));

  constructor(
    private dialogOpener: MatDialog,
    private commonService: CommonService,
    private utilityService: UtilityService,
    private dialogRef: MatDialogRef<WithdrawalDialogComponent>, 
    @Inject(MAT_DIALOG_DATA) public data: any) { }

  ngOnInit(): void {
    this.dialogRef.disableClose = true;
    this.parsedBalance = this.utilityService.parseNumberWithCommas(
      Number(this.data.account?.balance * this.selectedCurrency?.rate).toFixed(2)
    );
    this.form = new FormGroup({
      accountNumber: new FormControl('', Validators.required),
      amount: new FormControl('', Validators.required),
      bank: new FormControl('', Validators.required)
    });
    this.getCryptos();
  }

  close() {
    this.dialogRef.close();
  }

  // getBanks() {
  //   this.commonService.getBanks('bank')
  //   .subscribe(banks => {
  //     this.banks = banks;
  //   }, error => {
  //     this.dialogOpener.open(OkDialogComponent);
  //   });
  // }

  getCryptos() {
    this.commonService.getBanks('crypto')
    .subscribe(banks => {
      this.banks = banks;
    }, error => {
      this.dialogOpener.open(OkDialogComponent);
    });
  }

  withdraw(value: number = 0) {
    if (!value || value <= 0 ) {
      this.errorMessage = 'Please enter a valid withdrawal amount.';
      return;
    }
    if (value > this.data.account.balance) {
      this.errorMessage = 'Amount cannot be more than your balance.';
      return;
    }

    this.errorMessage = '';
    this.isWithdrawing = true;
    const timeoutWaitMs = (Math.floor(Math.random() * 5) + 1) * 1000;

    setTimeout(() => {
      this.isWithdrawing = false;
      this.dialogOpener.open(OkDialogComponent, {
        data: {
          title: 'Withdrawal Error!',
          message:
            `<span class="text-red-700">` +
            `Sorry, your withdrawal transaction could not be completed. ` +
            `Try again after some time. <a href="/contact-us" target="_blank" class="text-blue-600">` +
            `Contact us</a> if you continue to experience this issue.` +
            `</span>`,
        },
      });
    }, timeoutWaitMs);
  }

}
