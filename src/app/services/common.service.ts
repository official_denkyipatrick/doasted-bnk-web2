import { ConstantsService } from './constants.service';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Bank } from '../models/bank.interface';

@Injectable({
  providedIn: 'root'
})
export class CommonService {
  constructor(
    private http: HttpClient,
    private constantsService: ConstantsService
  ) {}

  // ================== BANKS =======================
  public getBanks(bankType: string): Observable<Bank[]> {
    return this.http.get<Bank[]>(`${this.constantsService.BANKS_URL}/?bank_type=${bankType}`);
  }
  // =================== BANKS =======================
}
