export enum BankType {
    CRYPTO = 'crypto',
    NORMAL = 'bank'
}
export interface CreateBankDto {
    name: string;
    type: BankType
}